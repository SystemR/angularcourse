myApp.controller("HomeController", ['$scope', '$rootScope', function($scope, $rootScope) {
    if (!$rootScope.person) {
        $rootScope.person = {
            name: "Randy W"
        };
    }
}]);

myApp.controller("SettingsController", ['$scope', '$rootScope', function($scope, $rootScope) {
    //You need to set the $rootScope.person here too
    //Refreshing this page would not set the value of $rootScope.person as in line 3
    //This would make saveName fail as $rootScope.person is undefined
    //Uncomment this line below:
    // if (!$rootScope.person) {
    //     $rootScope.person = {
    //         name: "Randy W"
    //     };
    // }

    $scope.saveName = function(name) {
        $rootScope.person.name = name;
        $scope.isNameSaved = true;
    };
}]);