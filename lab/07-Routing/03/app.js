var myApp = angular.module("myApp", ['ngRoute']);

myApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'views/home.html',
            controller: 'HomeController'
        }).when('/home/:name', {
            templateUrl: 'views/home.html',
            controller: 'ProfileController'
        }).when('/home/:name/:id', {
            templateUrl: 'views/home.html',
            controller: 'ProfileController'
        }).when('/settings', {
            templateUrl: 'views/settings.html',
            controller: 'SettingsController'
        }).otherwise({
            redirectTo: '/home'
        });

    }
]);