myApp.controller("HomeController", ['$scope', 'personService',
    function($scope, personService) {
        $scope.person = personService.get();
    }
]);

myApp.controller("ProfileController", ['$scope', 'personService', '$routeParams',
    function($scope, personService, $routeParams) {
        console.log($routeParams);

        //Change the name
        personService.setName($routeParams.name);

        $scope.person = personService.get();
        $scope.person.id = $routeParams.id;
    }
]);

myApp.controller("SettingsController", ['$scope', 'personService',
    function($scope, personService) {
        $scope.name = personService.get().name;

        $scope.saveName = function(name) {
            personService.setName(name);
            $scope.isNameSaved = true;
        };
    }
]);

myApp.factory("personService", function() {
    var person = {
        name: "Randy W"
    };

    return {
        setName: function(name) {
            person.name = name;
        },
        get: function() {
            return person;
        }
    };
});