myApp.controller("HomeController", ['$scope', 'personService',
    function($scope, personService) {
        $scope.person = personService.get();
    }
]);

myApp.controller("SettingsController", ['$scope', 'personService',
    function($scope, personService) {
        $scope.person = personService.get();

        $scope.saveName = function(name) {
            personService.setName(name);
            $scope.isNameSaved = true;
        };
    }
]);

myApp.factory("personService", function() {
    var person = {
        name: "Randy W"
    };

    return {
        setName: function(name) {
            person.name = name;
        },
        get: function(name) {
            return person;
        }
    };
});