function HelloController($scope) {
    $scope.person = {
        name: "My Name"
    };

    $scope.addGreeting = function() {
        $scope.person.name = $scope.person.name + ". Welcome to Angular Course";
    };

}