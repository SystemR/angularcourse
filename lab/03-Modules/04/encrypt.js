var encryptModule = angular.module("encryptModule", []);

/******************************************************************
 * Encrypt Filters
 ******************************************************************/
encryptModule.filter('encrypt', function() {
    var key = "SXGWLZPDOKFIVUHJYTQBNMACERxswgzldpkoifuvjhtybqmncare";

    function encodeStr(uncoded) {
        uncoded = uncoded.toUpperCase();
        var coded = "",
            chr;
        for (var i = uncoded.length - 1; i >= 0; i--) {
            chr = uncoded.charCodeAt(i);
            coded += key.charAt(chr - 65 + 26);
        }
        return encodeURIComponent(coded);
    }
    return function(text) {
        if (text) {
            return encodeStr(text);
        }
        return "";
    };
});

/******************************************************************
 * Encrypt Services
 ******************************************************************/
encryptModule.factory('encryptService', ['$http', function($http) {
    return {
        authenticateForDecrypt: function(user) {
            return $http.get("/auth", user);
        }
    };
}]);