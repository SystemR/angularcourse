var myApp = angular.module("myApp", []);

/******************************************************************
 * Controllers
 ******************************************************************/
myApp.controller('HelloController', ['$scope',
    function($scope) {
        $scope.person = {
            name: "My Name"
        };
    }
]);

/******************************************************************
 * Directives
 ******************************************************************/
myApp.directive('blinkDiv', function() {
    return function(scope, el, attr) {
        var parent = el.parent();
        var color = "red";
        el.on("keyup", function() {
            if (color == "red") {
                color = "blue";
            } else {
                color = "red";
            }

            parent.css({
                "border-color": color
            });
        });
    };
});