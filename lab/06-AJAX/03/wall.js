angular.module("wallModule", []).factory("wallService", ['$http', function($http) {
    return {
        //Returns an object with a data property containing wall posts
        get: function() {
            return $http.get("walls.json");
        }
    };
}]);