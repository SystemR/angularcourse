angular.module("yahooApi", []).factory("yahooApiService", ['$http', function($http) {
    return {
        get: function(query, location) {
            return $http({
                method: 'JSONP',
                url: 'http://query.yahooapis.com/v1/public/yql',
                params: {
                    callback: 'JSON_CALLBACK',
                    q: 'select * from local.search where query="'+ query +'" and location="' + location + '"',
                    format: "json"
                }
            });
        }
    };
}]);