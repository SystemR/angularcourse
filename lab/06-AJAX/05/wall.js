angular.module("wallModule", []).factory("wallService", ['$http', '$q',
    function($http, $q) {
        var cachedWallResponse = {
            "walls": [{
                "name": "Adib Saikali",
                "comment": "Messages received",
                "timestamp": "2014-05-24T17:45:23.331Z"
            }, {
                "name": "John Doe",
                "comment": "Bla bla bla",
                "timestamp": "2014-05-24T17:30:20.331Z"
            }, {
                "name": "Tardis",
                "comment": "Testing messages",
                "timestamp": "2014-05-24T17:35:23.331Z"
            }, {
                "name": "Tardis",
                "comment": "Hello World!",
                "timestamp": "2014-05-24T17:25:21.695Z"
            }],
            "status": 1
        };

        // var cachedWallResponse = null;
        return {
            //Response of this API is a full promise object (no .success or .error)
            //Returns an object with a data property containing wall posts

            /**********************************************************
             * Retrieve wall posts
             * @returns {Object} a promise object with wall data when resolved
             **********************************************************/
            get: function() {
                var dfd = $q.defer();
                var httpResponse = {};

                if (cachedWallResponse) {
                    httpResponse.data = cachedWallResponse;
                    dfd.resolve(httpResponse);
                } else {
                    $http.get("walls.json").success(function(data) {
                        httpResponse.data = data;
                        cachedWallResponse = data;
                        dfd.resolve(httpResponse);
                    });
                }
                return dfd.promise;
            }
        };
    }
]);