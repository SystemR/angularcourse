angular.module("wallModule", []).factory("wallService", ['$http', '$q', function($http, $q) {
    return {
        //Returns an object with a data property containing wall posts
        //We want to return a full promise object (no .success or .return)
        //This is so that it will be easier to change the API later
        get: function() {
            return $http({
                method: "POST",
                url: "walls.json"
            });
        }
    };
}]);