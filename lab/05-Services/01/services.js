function displayFileSize(sizeInBytes) {
    if (sizeInBytes) {
        var labels = ['bytes', 'KB', 'MB', 'GB', 'TB'],
            i, len, label = labels[0];
        for (i = 1, len = labels.length; i < len && sizeInBytes > 1024; i++) {
            sizeInBytes = sizeInBytes / 1024;
            label = labels[i];
        }
        return sizeInBytes.toFixed(2) + " " + label;
    }
    return "0 KB";
}

angular.module("myApp", []).factory("storageService", function() {
    var storageData = {
        bytes: 204800,
        numOfFiles: 20
    };

    return {
        set: function(storage) {
            storageData.bytes = storage;
        },
        get: function() {
            return storageData;
        },
        add: function(size) {
            storageData.bytes += size;
        },
        remove: function(size) {
            storageData.bytes -= size;
        }
    };
}).filter('fileSize', function() {
    return function(bytes) {
        return displayFileSize(bytes);
    };
});