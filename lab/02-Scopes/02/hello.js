function HelloController($scope, $rootScope) {
    $scope.person = {
        name: "My Name"
    };

    $rootScope.title = "Value in Root Scope";

    $scope.addGreeting = function() {
        $scope.person.name = $scope.person.name + ". Welcome to Angular Course";
    };

    $scope.copyToRootScope = function() {
        $rootScope.title = $scope.person.name;
    };

    $scope.clearName = function() {
        $scope.person.name = "";
    };
}