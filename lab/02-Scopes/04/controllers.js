function ParentController($scope, $rootScope) {
    $scope.person = {
        name: "Gepetto",
        childName: "Pinocchio"
    };

    $rootScope.title = "Value in Root Scope";
}

function ChildController($scope, $rootScope) {
    //You can access $scope.person here since the Angular will point
    //$scope.person to the object in line 2

    $scope.addGreeting = function() {
        $scope.person.name = $scope.person.name + ". Welcome to Angular Course";
    };

    $scope.copyToRootScope = function() {
        $rootScope.title = $scope.person.name;
    };

    $scope.clearName = function() {
        $scope.person.name = "";
    };
}