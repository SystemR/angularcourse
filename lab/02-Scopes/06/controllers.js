function ParentController($scope, $rootScope) {
    $scope.person = {
        name: "Gepetto",
        childName: "Pinocchio"
    };

    $scope.author = "Carlo";

    $rootScope.title = "Value in Root Scope";
}

function ChildController($scope, $rootScope) {
    //You can access $scope.person here since the Angular will point
    //$scope.person to the object in line 2

    $scope.addGreeting = function() {
        $scope.person.name = $scope.person.name + ". Welcome to Angular Course";
    };

    $scope.copyToRootScope = function() {
        $rootScope.title = $scope.person.name;
    };

    $scope.clearName = function() {
        $scope.person.name = "";
    };
}

//Upon instantiation of ChildController, $scope.author in the ChildController
//would still be pointing to the string in line 7. However, modifying $scope.author
//in the child scope will not get propagated to the parent since a new $scope.author
//string object will be created and get attached to the child scope

//This is the behavior of JavaScript prototypal inheritance.

//Long answer here: https://github.com/angular/angular.js/wiki/Understanding-Scopes

//TIPS: Use objects if you want to reference to the same object as parent
