function HelloController($scope) {
    $scope.person = {
        name: "Gepetto"
    };

    $scope.$watch('person.name', function(newVal, oldVal, scope) {
        console.log("New Val : " + newVal);
        console.log("Old Val : " + oldVal);
        console.dir(scope);
    });

    $scope.addGreeting = function() {
        $scope.person.name = $scope.person.name + ". Welcome to Angular Course";
    };

    $scope.clearName = function() {
        $scope.person.name = "";
    };
}