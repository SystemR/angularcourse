angular.module('WallApp.services', []).factory("wallService", ['$http',
    function($http) {
        var walls = [];
        return {
            list: function() {
                // LAB:
                // 1. Return a $http.get('backend/walls.json')
                // 2. Don't forget to set the walls on line 3 to data.walls from the
                // returned data
            },
            send: function(post) {
                return $http.get('backend/comment.json').success(function(data) {
                    if (data.status) {
                        walls.unshift(post);
                    }
                });
            }
        };
    }
]);