//Notice the wallService dependency being added to the controllers
angular.module("WallApp.controllers", []).controller('MainController', ['$scope', 'wallService',
    function($scope, wallService) {
        $scope.name = "Randy";

        //Use wallService to list wall posts and attach it to scope
        wallService.list().success(function(data) {
            $scope.walls = data.walls;
        });

        // Old way:
        // $scope.walls = [{
        //     name: $scope.name,
        //     comment: "Hello World",
        //     timestamp: new Date()
        // }];
    }
]).controller('WallSubmitController', ['$scope',
    function($scope) {
        $scope.send = function() {
            // LAB:
            // Use wallService.send() method to post data

            this.comment = "";
        };

        $scope.clear = function() {
            this.comment = "";
        };
    }
]);