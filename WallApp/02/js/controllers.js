angular.module("WallApp.controllers", []).controller('MainController', ['$scope',
    function($scope) {
        $scope.name = "Randy";
        $scope.walls = [{
            name: $scope.name,
            comment: "Hello World",
            timestamp: new Date()
        }];
    }
]).controller('WallSubmitController', ['$scope',
    function($scope) {
        $scope.send = function() {
            $scope.walls.unshift({
                name: $scope.name,
                comment: this.comment,
                timestamp: new Date()
            });
            this.comment = "";
        };

        $scope.clear = function() {
            this.comment = "";
        };
    }
]);