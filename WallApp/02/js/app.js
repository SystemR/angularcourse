angular.module('WallApp', [
    //Dependencies
    'WallApp.controllers',
    'WallApp.filters'
]);


/****************
 * Utility codes
 ****************/
var Util = {
    timeago: function (inDate) {
        if (typeof inDate == "string") {
            inDate = new Date(inDate);
        }
        if (!inDate || !inDate.getTime()) {
            return "never";
        }
        var distanceMillis = new Date().getTime() - inDate.getTime(),
            strings = {
                prefixAgo: null,
                prefixFromNow: null,
                suffixAgo: "ago",
                suffixFromNow: "from now",
                seconds: "seconds ",
                minute: "about a minute",
                minutes: "%d minutes",
                hour: "about an hour",
                hours: "about %d hours",
                day: "a day",
                days: "%d days",
                month: "about a month",
                months: "%d months",
                year: "about a year",
                years: "%d years",
                wordSeparator: " ",
                numbers: []
            }, prefix = strings.prefixAgo,
            suffix = strings.suffixAgo,
            seconds = Math.abs(distanceMillis) / 1000,
            minutes = seconds / 60,
            hours = minutes / 60,
            days = hours / 24,
            years = days / 365,
            words, separator;
        if (distanceMillis < 0) {
            prefix = strings.prefixFromNow;
            suffix = strings.suffixFromNow;
        }

        function substitute(stringOrFunction, number) {
            var string = stringOrFunction,
                value = (strings.numbers && strings.numbers[number]) || number;
            return string.replace(/%d/i, value);
        }

        words = seconds < 45 && substitute(strings.seconds, Math.round(seconds)) ||
            seconds < 90 && substitute(strings.minute, 1) ||
            minutes < 45 && substitute(strings.minutes, Math.round(minutes)) ||
            minutes < 90 && substitute(strings.hour, 1) ||
            hours < 24 && substitute(strings.hours, Math.round(hours)) ||
            hours < 42 && substitute(strings.day, 1) ||
            days < 30 && substitute(strings.days, Math.round(days)) ||
            days < 45 && substitute(strings.month, 1) ||
            days < 365 && substitute(strings.months, Math.round(days / 30)) ||
            years < 1.5 && substitute(strings.year, 1) ||
            substitute(strings.years, Math.round(years));

        separator = strings.wordSeparator || "";
        if (strings.wordSeparator === undefined) {
            separator = " ";
        }
        return [prefix, words, suffix].join(separator).trim();
    },
    getTopCommenter: function(walls) {
        //Returns the name of the person with the most number of comments
        var indexes = {};
        for (var i = 0, len = walls.length; i < len; i++) {
            var ptr = walls[i];
            if (!indexes[ptr.name]) {
                indexes[ptr.name] = 1;
            } else {
                indexes[ptr.name]++;
            }
        }

        var max = 0, maxName = "";
        for(var name in indexes) {
            if (indexes[name] > max) {
                maxName = name;
                max = indexes[name];
            }
        }
        return maxName;
    }
};