angular.module("WallApp.controllers", []).controller('MainController', ['$scope', 'wallService',
    function($scope, wallService) {
        $scope.name = "Randy";
        wallService.list().success(function(data) {
            $scope.walls = data.walls;
        });
    }
]).controller('WallSubmitController', ['$scope', 'wallService',
    function($scope, wallService) {
        $scope.name = $scope.name;

        $scope.send = function() {
            wallService.send({
                name: $scope.name,
                comment: this.comment,
                timestamp: new Date()
            });
            this.comment = "";
        };

        $scope.clear = function() {
            this.comment = "";
        };
    }
]);