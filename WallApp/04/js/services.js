angular.module('WallApp.services', []).factory("wallService", ['$http',
    function($http) {
        var walls = [];
        return {
            list: function() {
                return $http.get('backend/walls.json').success(function(data) {
                    if (data.status) {
                        walls = data.walls;
                    }
                });
            },
            send: function(post) {
                return $http.get('backend/comment.json').success(function(data) {
                    if (data.status) {
                        walls.unshift(post);
                    }
                });
            }
        };
    }
]);