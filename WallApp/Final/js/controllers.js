angular.module("WallApp.controllers", []).controller('MainController', ['$scope', '$location', 'wallService',
    function($scope, $location, wallService) {
        $scope.name = "Randy";

        wallService.list().then(function(walls) {
            $scope.walls = walls;
        });
        $scope.$location = $location;
    }
]).controller('WallSubmitController', ['$scope', 'wallService',
    function($scope, wallService) {
        $scope.send = function() {
            wallService.send({
                name: $scope.name,
                comment: this.comment,
                timestamp: new Date()
            }).then(function(response) {
                var data = response.data;
                if (data.status) {
                    $scope.sendSuccess = true;
                } else {
                    $scope.sendError = true;
                }

                setTimeout(function() {
                    $scope.sendSuccess = false;
                    $scope.sendError = false;
                }, 5000);
            });
            this.comment = "";
        };

        $scope.clear = function() {
            this.comment = "";
            $scope.sendSuccess = false;
            $scope.sendError = false;
        };
    }
]).controller('WallStatsController', ['$scope',
    function($scope) {
        $scope.topCommenter = function() {
            return Util.getTopCommenter($scope.walls);
        };
    }
]).controller('SettingsController', ['$scope', function($scope) {
    $scope.nameChangeSuccess = false;
    $scope.changeName = function(newName) {
        $scope.$parent.name = newName;
        $scope.nameChangeSuccess = true;
    };
}]);