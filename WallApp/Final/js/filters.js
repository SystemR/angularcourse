angular.module("WallApp.filters", []).filter('timeago', function() {
    return function(date) {
        return Util.timeago(date);
    };
});