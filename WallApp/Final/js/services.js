angular.module('WallApp.services', []).factory("wallService", ['$http', '$q',
    function($http, $q) {
        var walls = null;
        return {
            list: function() {
                var dfd = $q.defer();

                if (!walls) {
                    $http.get('backend/walls.json').success(function(data) {
                        if (data.status) {
                            walls = data.walls;
                            dfd.resolve(walls);
                        }
                    });
                } else {
                    dfd.resolve(walls);
                }

                return dfd.promise;
            },
            send: function(post) {
                return $http.get('backend/comment.json').success(function(data) {
                    if (data.status) {
                        walls.unshift(post);
                    }
                });
            }
        };
    }
]);