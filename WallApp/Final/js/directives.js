angular.module('WallApp.directives', []).directive('pageExpander', function() {
    return function(scope, el, attrs) {
        var $mainPanel = el.parent().parent();
        var $sidePanel = $mainPanel.next();
        var isShrinked = false;

        function showHide() {
            if (isShrinked) {
                $mainPanel.removeClass('col-sm-12').addClass('col-sm-8');
                $sidePanel.css({
                    "display": "block"
                });
                isShrinked = false;
            } else {
                $mainPanel.removeClass('col-sm-8').addClass('col-sm-12');
                $sidePanel.css({
                    "display": "none"
                });
                isShrinked = true;
            }
        }

        el.on("click", function() {
            showHide();
        });
    };
});