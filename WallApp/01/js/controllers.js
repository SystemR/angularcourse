angular.module("WallApp.controllers", []).controller('MainController', ['$scope',
    function($scope) {
        $scope.name = "Randy";
        $scope.walls = [{
            name: $scope.name,
            comment: "Hello World",
            timestamp: new Date()
        }];
    }
]).controller('WallSubmitController', ['$scope',
    function($scope) {
        $scope.send = function() {
            // LAB:
            // Add a code to walls.unshift() a new comment

            //Clear comment
            this.comment = "";
        };

        $scope.clear = function() {
            // LAB:
            // Add a code to clear the input
        };
    }
]);

// push() 
// unshift();
// pop();
// shift();